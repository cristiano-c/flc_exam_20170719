# Formal Languages and Compilers
> Correzione dell'elaborato del 19 luglio 2017

Nella `root` di questa repo si trova la correzione dell'elaborato svolto il 19 luglio 2017.

L'elaborato consegnato *senza modifiche* è contenuto nell'archivio compresso: `original/flc_20170719.zip`.

----

È possibile osservare –  attraverso il sistema di tracking dei commit posizionandosi nella branch [correzione](https://gitlab.com/cristiano-c/flc_exam_20170719/tree/correzione) – nel dettaglio tutte le modifiche apportate in quanto la correzione è stata effettuata sull'elaborato originale.
