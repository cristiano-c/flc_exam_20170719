numExpectedConflicts = 33
mode = 0 //Mode to launch jflex debug {0,1,2}

default:
	rm -f parser.java Scanner.java sym.java
	rm -f *.class
	rm -f *.*~
	jflex scanner.jflex
	java java_cup.Main -expect $(numExpectedConflicts) -parser parser parser.cup
	javac *.java

clean:
	rm -f parser.java Scanner.java sym.java
	rm -f *.class
	rm -f *.*~

run:
	java Main input.txt

roborun:
	java Main input.txt $(mode)