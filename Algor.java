import java.util.LinkedList;
import java.util.Map;
import java.util.TreeMap;

/**
 * Created by Fenix on 25/06/17.
 */
public class Algor
{
    private class demo
    {
        private LinkedList<String> linkedLista = new LinkedList<>();
        private TreeMap<String, String> treeMap = new TreeMap<>();
        private TreeMap<String, Integer> treeMapInt = new TreeMap<>();
        private StringBuffer stringBuffer = new StringBuffer();

        public void linkedListaMethod()
        {
            this.linkedLista.add("2");
            this.linkedLista.add("1");
            this.linkedLista.get(0);
            this.linkedLista.size();
            this.linkedLista.remove(0);
            this.linkedLista.removeLast();
            this.linkedLista.getLast();
            this.linkedLista.getFirst();

            for (int i = 0; i < this.linkedLista.size(); i++) ;

        }

        public void setTreeMapMethod()
        {
            this.treeMap.put("key", "value");
            this.treeMap.size();
            this.treeMap.containsKey("key");
            this.treeMap.remove("key");
            this.treeMap.replace("key", "old value", "new value");

            for (Map.Entry<String, String> entry : treeMap.entrySet())
            {
                String key = entry.getKey();
                String value = entry.getValue();
                System.out.println(key + " => " + value);
            }

            for (Map.Entry<String, Integer> entry : treeMapInt.entrySet())
            {
                String key = entry.getKey();
                Integer value = entry.getValue();
                System.out.println(key + " => " + value);
            }

        }

        public void stringBufferMethod()
        {
            StringBuffer strTemp = new StringBuffer();
            strTemp.append("1");
            strTemp.append(" ");
            strTemp.append("3");
            String[] split = strTemp.toString().split(" ");
            for (int i = 0; i < split.length; i++)
            {
                String l = split[i];
            }
            strTemp.length();
            strTemp.toString();
            strTemp.charAt(0); //Ottieni l'elemento dato l'indice;
        }

        public void compareToMethod()
        {
            if (stringBuffer.toString().compareTo("ciao") == 0)
            {
                System.out.println("Elemento trovato");
            } else
            {
                System.out.println("Elemento NON trovato");
            }
        }
    }


}
