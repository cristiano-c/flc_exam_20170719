// Copyright 2017 Cristiano Cavo
import java.io.*;
public class Main {
  static public void main(String argv[]) {
    try {
        /* Scanner instantiation */
        Scanner l = new Scanner(new FileReader(argv[0]));
        if(argv.length == 2){
            if(argv[1].equals("0") || argv[1].equals("1") || argv[1].equals("2")) l.setMode(argv[1]);
            else l.setMode("0");
        } else {
            l.setMode("0");
        }
        /* Parser instantiation */
        parser p = new parser(l);
        /* Start the parser */
      Object result = p.parse();
    } catch (Exception e) {
      e.printStackTrace();
    }
  }
}
