import java_cup.runtime.*;

%%

// 'class scanner': indica il nome che avrà la classe dello scanner (JFLex)
%class Scanner

//%standalone

// 'unicode': set di caratteri ASCII che ci si aspetta di trovare nell'input
%unicode

// 'cup': direttiva per indicare che JFLex viene collegato a CUP
%cup

// Line e column servono per individuare riga e colonna del carattere che attualmente si sta scansionando (utile in questo esercizio per indicare DOVE si è verificato un errore di riconoscimento)
%line
%column
// 32 = verde
// 31 = rosso
// Permette di elaborare le posizioni di riga e colonna su CUP (ritorna il simbolo attuale!)
%{
 private int i = 0;
    private int debugMode = 2;

    // Imposta la modalità di stampa
    public void setMode(String val) {
        debugMode = Integer.parseInt(val);
    }

    private Symbol symbol(int type) {
        return new Symbol(type, yyline, yycolumn);
    }

    private Symbol symbol(int type, Object value) {
        return new Symbol(type, yyline, yycolumn, value);
    }

    private void hit(String msg) {
        switch(debugMode){
            // Modalità silenziosa
            case 0:
            break;

            // Modalità debug
            case 1: System.out.print("\u001B[32m" + msg +"\u001B[0m");
            break;

            // Modalità debug verbose
            case 2: System.out.println("\u001B[32m[OK] [" + msg + "]\u001B[0m");
            break;

        }
    }

    private void miss(String msg) {
        switch(debugMode){
            // Modalità silenziosa
            case 0:
            break;

            // Modalità debug
            case 1: System.out.print("\u001B[31m" + msg + "\u001B[0m");
            break;

            // Modalità debug verbose
            case 2: System.out.println("\u001B[31m[ERR] [" + msg + "]" + "\u001B[0m");
            break;

        }
    }

    private void hit(String id, String msg) {

        switch(debugMode){
            // Modalità silenziosa
            case 0:
            break;

            // Modalità debug
            case 1: System.out.print("\u001B[32m" + msg + "\u001B[0m");
            break;

            // Modalità debug verbose
            case 2: System.out.println("\u001B[32m" + "[" + id + "] [" + msg + "]" + "\u001B[0m");
            break;

        }
    }

    private void miss(String id, String msg) {

        switch(debugMode){
            // Modalità silenziosa
            case 0:
            break;

            // Modalità debug
            case 1: System.out.print("\u001B[31m" + msg + "\u001B[0m");
            break;

            // Modalità debug verbose
            case 2: System.out.println("\u001B[31m"  + "[" + id + "] [" + msg + "]" + "\u001B[0m");
            break;

        }
    }
    /* Funzioni overloaded */

    private void hit(String msg, int modeRun) {
      int i = debugMode;
      debugMode = modeRun;
        switch(debugMode){
            // Modalità silenziosa
            case 0:
            break;

            // Modalità debug
            case 1: System.out.print("\u001B[32m[" + msg +"]\u001B[0m");
            break;

            // Modalità debug verbose
            case 2: System.out.println("\u001B[32m[OK] [" + msg + "]\u001B[0m");
            break;

        }
          debugMode=i;
    }

      private void miss(String msg, int modeRun) {
        int i = debugMode;
        debugMode = modeRun;

        switch(debugMode){
            // Modalità silenziosa
            case 0:
            break;

            // Modalità debug
            case 1: System.out.print("\u001B[31m" + msg + "\u001B[0m");
            break;

            // Modalità debug verbose
            case 2: System.out.println("\u001B[31m[ERR] [" + msg + "]" + "\u001B[0m");
            break;

        }
          debugMode=i;
    }

    private void hit(String id, String msg, int modeRun) {
        int i = debugMode;
        debugMode = modeRun;
        switch(debugMode){
            // Modalità silenziosa
            case 0:
            break;

            // Modalità debug
            case 1: System.out.print("\u001B[32m" + msg + "\u001B[0m");
            break;

            // Modalità debug verbose
            case 2: System.out.println("\u001B[32m" + "[" + id + "] [" + msg + "]" + "\u001B[0m");
            break;

        }
          debugMode=i;
    }

    private void miss(String id, String msg, int modeRun) {
      int i = debugMode;
      debugMode = modeRun;

        switch(debugMode){
            // Modalità silenziosa
            case 0:
            break;

            // Modalità debug
            case 1: System.out.print("\u001B[31m" + msg + "\u001B[0m");
            break;

            // Modalità debug verbose
            case 2: System.out.println("\u001B[31m"  + "[" + id + "] [" + msg + "]" + "\u001B[0m");
            break;

        }
        debugMode=i;
    }

%}

// '%state COMMENT': una direttiva che avvisa che ci sara' uno stato chiamato 'COMMENT'

nl = \n|\r|\r\n
ws = [ \t]+
blank = {nl}|{ws}
digit = [0-9]
sep = ###(#)* /***/
comment = "/*" ~ "*/" /**/
sc = \; /***/
token11 = \?0*10*10*10*10*
token12 = \?y?(xy)*y?
token13 = \?x?(yx)*x?
token1 = {token11}|{token12}|{token13} /***/
hour = (":"01":"((1[2-9])|([2-5][0-9])))|(":"((0[2-9])|(10))":"[0-5][0-9])|(":"11":"[0-2][0-9])|(":"11":"3[0-7])
token2 = (2017\/01\/((1[8-9])|(2[0-9])|(3[0-1])))|(2017\/01\/(([0-1][0-9])|(2[0-8]))) |(2017\/0[3-6]\/(([0-2][0-9])|(3[0-1])))|(2017\/07\/(0[0-2]))({hour}?) /***/
NumeroDispari = (15|17|19|[2-9]{Cifradispari})|([1-9][0-9]{Cifradispari})|(1[0-4][0-9]{Cifradispari})|(15[0-6]{Cifradispari})|(157(1|3)) /**/
confkw = "CONFIGURE" /***/
tempkw = "TEMPERATURE" /***/
humkw = "HUMIDITY"  /***/
storekw = "STORE" /***/
casekw = "CASE" /***/
inkw = "IN" /***/
rangekw = "RANGE" /*String*/
equalkw = "EQUAL" /*String*/
iskw = "IS" /***/
avgkw = "AVG" | "avg" /***/
equal = \= /***/
minus = \- /***/
slash = \/ /***/
ast = \* /***/
oround = \( /***/
cround = \) /***/
comma = \, /***/
powop = \^ /***/
obracket = \{ /***/
cbracket = \} /***/

Cifradispari = (1|3|5|7|9)
Cifrapari = (0|2|4|6|8)
wordsep = (\/|\$|{plus})
word = ({NumeroDispari}{wordsep}{NumeroDispari}{wordsep}{NumeroDispari}{wordsep}{NumeroDispari}{wordsep}{NumeroDispari}{wordsep}{NumeroDispari})({wordsep}{NumeroDispari}{wordsep}{NumeroDispari})*
token3 = {word} /***/

CifraDecimale = {Cifrapari}|{Cifradispari}
NumeroInteroUnsigned = 0 | [1-9]{CifraDecimale}*
NumeroInteroPositivo = {NumeroInteroUnsigned}
numint = {NumeroInteroPositivo} /*Integer*/
plus = \+ /***/
varname = (\_|[A-Z]|[a-z])(\_|[a-z]|[A-Z]|[0-9])* /*String*/

%%
/*protect_begin*/
// Solo esempio come inizio template (testare con debug 2), poi usare questo se si vogliono accettare le newlines
// {blank} {hit("BLANK", yytext()); return symbol(sym.BLANK);}
//{blank} {System.out.println();}
{blank} {hit( yytext());}
/*protect_end*/

/*out*/
{sep} {hit("SEP", yytext()); return symbol(sym.SEP);}
{comment} {hit("COMMENT", yytext());}
{sc} {hit("SC", yytext()); return symbol(sym.SC);}
{token1} {hit("TOKEN1", yytext()); return symbol(sym.TOKEN1);}
{token2} {hit("TOKEN2", yytext()); return symbol(sym.TOKEN2);}
{NumeroDispari} {hit("NUMERODISPARI", yytext());}
{confkw} {hit("CONFKW", yytext()); return symbol(sym.CONFKW);}
{tempkw} {hit("TEMPKW", yytext()); return symbol(sym.TEMPKW);}
{humkw} {hit("HUMKW", yytext()); return symbol(sym.HUMKW);}
{storekw} {hit("STOREKW", yytext()); return symbol(sym.STOREKW);}
{casekw} {hit("CASEKW", yytext()); return symbol(sym.CASEKW);}
{inkw} {hit("INKW", yytext()); return symbol(sym.INKW);}
{rangekw} {hit("RANGEKW", yytext()); return symbol(sym.RANGEKW, new String(yytext()));}
{equalkw} {hit("EQUALKW", yytext()); return symbol(sym.EQUALKW, new String(yytext()));}
{iskw} {hit("ISKW", yytext()); return symbol(sym.ISKW);}
{avgkw} {hit("AVGKW", yytext()); return symbol(sym.AVGKW);}
{equal} {hit("EQUAL", yytext()); return symbol(sym.EQUAL);}
{minus} {hit("MINUS", yytext()); return symbol(sym.MINUS);}
{slash} {hit("SLASH", yytext()); return symbol(sym.SLASH);}
{ast} {hit("AST", yytext()); return symbol(sym.AST);}
{oround} {hit("OROUND", yytext()); return symbol(sym.OROUND);}
{cround} {hit("CROUND", yytext()); return symbol(sym.CROUND);}
{comma} {hit("COMMA", yytext()); return symbol(sym.COMMA);}
{powop} {hit("POWOP", yytext()); return symbol(sym.POWOP);}
{obracket} {hit("OBRACKET", yytext()); return symbol(sym.OBRACKET);}
{cbracket} {hit("CBRACKET", yytext()); return symbol(sym.CBRACKET);}
{token3} {hit("TOKEN3", yytext()); return symbol(sym.TOKEN3);}
{numint} {hit("NUMINT", yytext()); return symbol(sym.NUMINT, new Integer(yytext()));}
{plus} {hit("PLUS", yytext()); return symbol(sym.PLUS);}
{varname} {hit("VARNAME", yytext()); return symbol(sym.VARNAME, new String(yytext()));}

/*protect_begin*/
. {miss("UNHANDLED", yytext());}
/*protect_end*/
